#include "Histo1D.h"

#include "HistoRms.h"
#include "HistoGaus.h"
#include "HistoOccupancy.h"
#include "HistoNoiseOccupancy.h"

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <algorithm> //for std::sort()

#include <iomanip>
#include <unistd.h>

#include <iostream>
#include <fstream>

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

///////////////////////////////////////////////////////////////////////////
/// Class for 1D histogram

Histo1D::Histo1D()
    : m_percent( 95.0 )
{
#ifdef DEBUG
    std::cout << "Histo1D::Histo1D()" << std::endl;
#endif
    m_latex_pos = { 0.28, 0.96, 0.8, 0.96 };
};

Histo1D::~Histo1D(){
#ifdef DEBUG
    std::cout << "Histo1D::~Histo1D()" << std::endl;
#endif
};

void Histo1D::buildHisto() {
#ifdef DEBUG
    std::cout << "Histo1D::buildHisto()" << std::endl;
#endif
    int nbinsx             = m_axis.histo.x.nbins;
    float xlow             = m_axis.histo.x.low;
    float xhigh            = m_axis.histo.x.high;
    std::string xaxistitle = m_axis.histo.x.title;
    std::string yaxistitle = m_axis.histo.y.title;
    std::string zaxistitle = m_axis.histo.z.title;

    m_h = new TH1F(m_histo_name.c_str(), "", nbinsx, xlow, xhigh);
    style_TH1((TH1*)m_h, xaxistitle.c_str(), yaxistitle.c_str(), kBlue);

    if (m_axis.histo.x.label.size()!=0) {
        for (int i=0; i<(int)m_axis.histo.x.label.size(); i++) m_h->GetXaxis()->SetBinLabel(i+1, m_axis.histo.x.label[i].c_str());
        m_h->GetXaxis()->LabelsOption("h");
        m_h->SetMarkerSize(1.8);
        m_h->SetMarkerColor(1);
    }
};

void Histo1D::setCanvas() {
#ifdef DEBUG
    std::cout << "Histo1D::setCanvas()" << std::endl;
#endif
    m_c = new TCanvas(Form("canv_%s",m_h->GetName()), "", 800, 600);
    style_TH1canvas(m_c);
};

void Histo1D::drawHisto() {
#ifdef DEBUG
    std::cout << "Histo1D::drawHisto()" << std::endl;
#endif
    style_TH1((TH1*)m_h, m_h->GetXaxis()->GetTitle(), m_h->GetYaxis()->GetTitle(), kBlue);
    if (m_axis.overwrite) {
        m_h->GetXaxis()->SetRangeUser(m_axis.histo.x.low, m_axis.histo.x.high);
        m_h->GetYaxis()->SetRangeUser(m_axis.histo.y.low, m_axis.histo.y.high);
    } else {
        m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.5)); //Leave extra room for legend
    }
    m_h->Draw();
};

void Histo1D::setParameters(json& j) {
#ifdef DEBUG
    std::cout << "Histo1D::setParameters(json)" << std::endl;
#endif
    PlotTool::setParameters(j);
}
void Histo1D::setParameters(TH1* h) {
#ifdef DEBUG
    std::cout << "Histo1D::setParameters(TH1)" << std::endl;
#endif
    PlotTool::setParameters(h);
}
void Histo1D::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "Histo1D::setParameters(TH2)" << std::endl;
#endif
    PlotTool::setParameters(h);
}

void Histo1D::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "Histo1D::fillHisto(j)" << std::endl;
#endif
    int nbinsx = j["x"]["Bins"];
    for (int i=0; i<nbinsx; i++) {
        double tmp = j["Data"][i];
        m_h->SetBinContent(i+1,tmp);
    }
};
void Histo1D::fillHisto(TH1* h) {
#ifdef DEBUG
    std::cout << "Histo1D::fillHisto(h)" << std::endl;
#endif
    int nbinsx = h->GetNbinsX();
    for (int i=0; i<nbinsx; i++) {
        double tmp = h->GetBinContent(i+1);
        double par = h->GetBinCenter(i+1);
        m_h->Fill(par, tmp);
    }
};
void Histo1D::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "Histo1D::fillHisto(h)" << std::endl;
#endif
};

void Histo1D::fitHisto(TH1* h) {
#ifdef DEBUG
    std::cout << "Histo1D::fitHisto(TH1)" << std::endl;
#endif
    TCanvas *c = new TCanvas(Form("c_fit_%s",h->GetName()), "", 800, 600);
    style_TH1canvas(c);
    h->Draw();
    double mean = h->GetMean();
    double rms = h->GetRMS();

    //Fit plot with Gaussian; for single FE plots
    std::string name = Form("f_%s", h->GetName());
    std::replace(name.begin(), name.end(), '-', '_');
    m_f = new TF1(name.c_str(), "gaus", (mean-5*rms < 0) ? -0.5 : (mean-5*rms), mean+5*rms);
    m_f->SetParameter(0, h->GetEntries());
    m_f->SetParameter(1, mean);
    m_f->SetParameter(2, rms);
    h->Fit(name.c_str(), "0Q", "", (mean-5*rms < 0) ? -0.5 : (mean-5*rms), mean+5*rms);
    //Change range and refit 5 times to get better fit.
    double fit_par[3];
    //double fit_err[3];
    for (int j=0; j<3; j++) {
        for (int k = 0; k<3; k++) {
            fit_par[k] = m_f->GetParameter(k);
            //fit_err[k] = m_f->GetParError(k);
        }
        double fit_min = fit_par[1] - (2*fit_par[2]);
        double fit_max = fit_par[1] + (2*fit_par[2]);
        h->Fit(name.c_str(), "0Q", "", fit_min, fit_max);
    }
    delete c;
}

json Histo1D::setOcc() {
#ifdef DEBUG
    std::cout << "Histo1D::setOcc()" << std::endl;
#endif
    json result;
    return result;
};

json Histo1D::setNoi() {
#ifdef DEBUG
    std::cout << "Histo1D::setNoi()" << std::endl;
#endif
    json result;
    return result;
};

json Histo1D::setRms(double mean, double sigma) {
#ifdef DEBUG
    std::cout << "Histo1D::setRms(" << mean << ", " << sigma << ")" << std::endl;
#endif
    std::string mean_str = Form("Mean_{hist} = %.1f", mean);
    this->setTLatexLeft(mean_str);
    std::string rms_str = Form("RMS_{hist} = %.1f", sigma);
    this->setTLatexLeft(rms_str);

    json result;
    result["mean"] = mean;
    result["sigma"] = sigma;
    return result;
};

json Histo1D::setZero() {
#ifdef DEBUG
    std::cout << "Histo1D::setZero()" << std::endl;
#endif
    std::string zeros_str = Form("Untuned Pixels = %i", m_zeros);
    this->setTLatexRight(zeros_str);

    json result;
    result["cnt"] = m_zeros;
    return result;
};

json Histo1D::setGaus(TF1* i_f) {
#ifdef DEBUG
    std::cout << "Histo1D::setGaus(TF1)" << std::endl;
#endif
    /// Write Gaus measurement
    double fit_par[3], fit_err[3];
    for (int j=0; j<3; j++) {
        for (int k = 0; k<3; k++) {
            fit_par[k] = i_f->GetParameter(k);
            fit_err[k] = i_f->GetParError(k);
        }
    }

    json result;
    if (fit_par[1] > 0) {
        std::vector <float> results;
        results = this->thresholdPercent(fit_par[1]);

        std::string mean_gaus_str  = Form("Mean_{gaus} = %.1f #pm %.1f", fit_par[1], fit_err[1]);
        this->setTLatexLeft(mean_gaus_str);
        std::string sigma_gaus_str = Form("#sigma_{gaus} = %.1f #pm %.1f", fit_par[2], fit_err[2]);
        this->setTLatexLeft(sigma_gaus_str);
        std::string chi_square_str = Form("#chi^{2} / DOF = %.1f / %i", i_f->GetChisquare(), i_f->GetNDF());
        this->setTLatexLeft(chi_square_str);
        std::string minmax_str  = Form("( %.2f / %.2f )_{%.1f%%}", results[1], results[3], m_percent);
        this->setTLatexRight(minmax_str);

        result["per"] = m_percent;
        result["min"] = results[1];
        result["max"] = results[3];
        result["zero"]["cnt"] = results[5];
        result["over"]["cnt"] = results[6];
    }

    result["mean"] = fit_par[1];
    result["sigma"] = fit_par[2];
    result["chi2"] = i_f->GetChisquare();
    return result;
};

std::vector <float> Histo1D::thresholdPercent(double gaussMean) {
    int nbinsx = m_h2->GetNbinsX();
    int nbinsy = m_h2->GetNbinsY();
    int zeroCounter = 0;
    int overCounter = 0;
    std::vector < std::pair<float,float> > mean_Diff;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double pix_value = m_h2->GetBinContent(col+1, row+1);
            if (pix_value<=0) zeroCounter++;
            else if (pix_value > 5*gaussMean) overCounter++;
            else mean_Diff.push_back( std::make_pair(pix_value, (fabs(pix_value-gaussMean))) );
        }
    }
    float numPixels = nbinsx*nbinsy;
    int numPix = (numPixels - (zeroCounter+overCounter)) * m_percent * 0.01;

    std::sort (mean_Diff.begin(), mean_Diff.end(), sortbysec); //sort from least to greatest for the second element of the pair.

    float vectorSize = mean_Diff.size();

    //Find the value corresponding to X% num of pix. Look at the values around X% of pixels and choose the min/max from there. Save how many iterations it took.
    float diffMax=99999999.0, diffMin=999999999.0, tryMax=0., tryMin=0.;
    for (int i=0; i<20000; i++) {
        if (mean_Diff[numPix-i].first > gaussMean) {
            diffMax = mean_Diff[numPix-i].first;
            tryMax=i*-1;
            break;
        }
        else if (mean_Diff[numPix+i].first > gaussMean && numPix+i < vectorSize) {
            diffMax = mean_Diff[numPix+i].first;
            tryMax=i;
            break;
        }
    }

    for (int i=0; i<20000; i++) {
        if (mean_Diff[numPix-i].first < gaussMean) {
            diffMin = mean_Diff[numPix-i].first;
            tryMin=i*(-1);
            break;
        }
        else if (mean_Diff[numPix+i].first < gaussMean && numPix+i < vectorSize) {
            diffMin = mean_Diff[numPix+i].first;
            tryMin=i;
            break;
        }
    }

    float checkSize = numPixels - (zeroCounter+overCounter);
    if ( vectorSize != checkSize ) {
        diffMax = -100000;
        diffMin = -100000;
    }
    float maxMin[7] = {vectorSize, diffMin, tryMin, diffMax, tryMax, (float)zeroCounter, (float)overCounter};
    std::vector <float> results;
    for (int i=0; i<7; i++) {
            results.push_back(maxMin[i]);
    }

    return results;
}

bool sortbysec(const std::pair<double, float> &a, const std::pair<double, float> &b) {	//sort using second element of pairs in vector.
        return (a.second < b.second);
}

json Histo1D::getResult() {
#ifdef DEBUG
    std::cout << "Histo1D::getResult()" << std::endl;
#endif
    json result;
    result["entry"] = m_h->GetEntries();

    return result;
}
