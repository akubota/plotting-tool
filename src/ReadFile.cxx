#include "ReadFile.h"

/////////////////////////////////
/// convert dat file to json file
json datToJson(std::string filepath) {
#ifdef DEBUG
    std::cout << "datToJson : " << filepath << std::endl;
#endif
    json j;
    std::fstream chipDataFile(filepath, std::ios::in);

    std::string type;
    std::string name;
    std::string xaxistitle, yaxistitle, zaxistitle;

    int nbinsx, nbinsy;
    double xlow, ylow;
    double xhigh, yhigh;
    int underflow, overflow;

    std::getline(chipDataFile, type);
    std::getline(chipDataFile, name);
    std::getline(chipDataFile, xaxistitle);
    std::getline(chipDataFile, yaxistitle);
    std::getline(chipDataFile, zaxistitle);

    type = type.substr(0,7); // EOL char kept by getline()

    if (type!="Histo1d"&&type!="Histo2d"&&type!="Histo3d") {
        std::cerr << "[ERROR] Something wrong with file: " << filepath << std::endl;
        j["is_null"] = true;
        return j;
    }
    j["Type"] = type;
    j["Name"] = name;
    j["x"]["AxisTitle"] = xaxistitle;
    j["y"]["AxisTitle"] = yaxistitle;
    j["z"]["AxisTitle"] = zaxistitle;

    chipDataFile >> nbinsx >> xlow >> xhigh;
    j["x"]["Bins"] = nbinsx;
    j["x"]["Low"]  = xlow;
    j["x"]["High"] = xhigh;

    if (type == "Histo2d") {
        chipDataFile >> nbinsy >> ylow >> yhigh;
        j["y"]["Bins"] = nbinsy;
        j["y"]["Low"]  = ylow;
        j["y"]["High"] = yhigh;
    } else {
        nbinsy = 1;
        j["y"]["Bins"] = nbinsy;
        j["y"]["Low"]  = 0;
        j["y"]["High"] = 1;
    }

    chipDataFile >> underflow >> overflow;
    j["Underflow"] = underflow;
    j["Overflow"] = overflow;

    if (!chipDataFile) {
        std::cerr << "[ERROR] Something wrong with file: " << filepath << std::endl;
        j["is_null"] = true;
        return j;
    }

    //json data;
    double tmp;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            chipDataFile>>tmp;
            if (!chipDataFile) {
                std::cerr << "[ERROR] Something wrong with file: " << filepath << std::endl;
                j["is_null"] = true;
                return j;
            }
            //j["Data"][col].push_back(tmp);
            if (type == "Histo2d") {
                j["Data"][col][row] = tmp;
            } else {
                j["Data"][col] = tmp;
            }
        }
    }

    std::ofstream ofs("data.json");
    ofs << std::setw(4) << j;

    return j;
}

///////////////////
/// scanLog to json
json readScanLog(json& i_j) {
#ifdef DEBUG
    std::cout << "readScanLog(json)" << std::endl;
#endif
    json log_json = i_j;
    json chip_json;
    std::string chip_type;
    if (log_json["connectivity"].type()==json::value_t::array) {
        for (auto conn_cfg : log_json["connectivity"]) { // check connectivity
            chip_type = conn_cfg["chipType"];
            for (auto chip : conn_cfg["chips"]) {
                chip_json.push_back(chip);
            }
        }
    } else {
        chip_type = log_json["connectivity"]["chipType"];
        chip_json = log_json["connectivity"]["chips"];
    }

    json j;
    if (chip_type=="FEI4B") chip_type = "FE-I4B";
    j["chipType"] = chip_type;
    j["chips"] = json::array();
    for (auto chip : chip_json) {
        std::string cfg_path = chip["config"];
        if (cfg_path.find("/")!=std::string::npos) cfg_path = cfg_path.substr(cfg_path.find_last_of("/")+1);
        chip["config"] = cfg_path;
        j["chips"].push_back(chip);
    }

    std::string key[3] = { "testType", "targetCharge", "targetTot" };
    for (int i=0; i<3; i++) {
        if (log_json[key[i]].empty()) {
            std::cerr << "Could not read '" << key[i] << "' from scanLog.json then abort." << std::endl;
            std::abort();
        }
        j[key[i]] = log_json[key[i]];
    }
    return j;
}

///////////////////
/// chipCfg to json
json readChipGlobCfg(json& i_j) {
#ifdef DEBUG
    std::cout << "readChipGlobCfg(json)" << std::endl;
#endif
    json file_json = i_j;

    for (auto& j : file_json.items()) {
        if (file_json[j.key()].is_object()) file_json[j.key()].erase("PixelConfig");
    }

    return file_json;
}

////////////////
/// file to json
json fileToJson(std::string filepath, std::string filetype) {
#ifdef DEBUG
    std::cout << "fileToJson(" << filepath << ", " << filetype << ")" << std::endl;
#endif
    json j = {{ "is_null", true }};
    struct stat filestat;
    if (stat(filepath.c_str(), &filestat)) { //skip if file is invalid
        std::cerr << "[ERROR] Not found the file: " << filepath << std::endl;
        std::abort();
    }
    std::ifstream file_ifs(filepath);
    try {
        j = json::parse(file_ifs);
    } catch (json::parse_error &e) {
        std::cerr << "[ERROR] Could not parse the file: " << filepath << std::endl;
        std::cerr << "        what(): " << e.what() << std::endl;
        std::abort();
    }
    j["is_null"] = false;

    json jout;
    if (filetype=="scanLog") {
        jout = readScanLog(j);
    } else if (filetype=="scanCfg") {
        jout = j;
    } else if (filetype=="plotCfg") {
        jout = j;
    } else if (filetype=="beforeCfg"||filetype=="afterCfg") {
        jout = readChipGlobCfg(j);
    } else {
        jout = j;
    }

    jout["is_null"] = false;
    return jout;
}

/// check chip config
json readChipPixCfg(std::string filepath, std::string chip_type, std::string type) {
#ifdef DEBUG
    std::cout << "readChipPixCfg : " << filepath << " chipType : " << chip_type << " configType: " << type << std::endl;
#endif
    json j = {{ "is_null", true }};
    struct stat filestat;
    if (stat(filepath.c_str(), &filestat)) { //skip if file is invalid
        return j;
    }

    std::ifstream file_ifs(filepath);
    try {
        j = json::parse(file_ifs);
    } catch (json::parse_error &e) {
        std::cerr << "[ERROR] Could not parse the file: " << filepath << std::endl;
        std::cerr << "        what(): " << e.what() << std::endl;
        return j;
    }

    std::string chip_name = "JohnDoe";
    if (chip_type=="FE-I4B") chip_name = j[chip_type]["name"];
    else if (chip_type=="RD53A") chip_name = j[chip_type]["Parameter"]["Name"];

    json pixCfg;
    bool lineRow;
    int line=0;
    int colnum, rownum;
    for (auto& i: j[chip_type]["PixelConfig"]) {
        if (i.count("Row")==1||i.count("row")==1) lineRow = true;
        else if (i.count("Col")==1||i.count("col")==1) lineRow = false;

        for (auto& el: i.items()) {
            if (el.key()=="Row"||el.key()=="row"||el.key()=="Col"||el.key()=="col") continue;
            else if (pixCfg.count(el.key())==0) {
                json cfg;
                cfg["Type"] = "Histo2d";
                cfg["Name"] = type+"_"+el.key();
                cfg["x"]["AxisTitle"] = "Column";
                cfg["y"]["AxisTitle"] = "Row";
                cfg["z"]["AxisTitle"] = el.key();
                pixCfg[el.key()] = cfg;
            }
            else if (pixCfg[el.key()]["x"].count("Bins")==0&&pixCfg[el.key()]["y"].count("Bins")==0) {
                pixCfg[el.key()]["x"]["Bins"] = colnum;
                pixCfg[el.key()]["x"]["Low"]  = 0.5;
                pixCfg[el.key()]["x"]["High"] = colnum+0.5;
                pixCfg[el.key()]["y"]["Bins"] = rownum;
                pixCfg[el.key()]["y"]["Low"]  = 0.5;
                pixCfg[el.key()]["y"]["High"] = rownum+0.5;
                pixCfg[el.key()]["Underflow"] = 0;
                pixCfg[el.key()]["Overflow"] = 0;
            }
            if (lineRow) {
                int col = 0;
                for (auto tmp: el.value()) {
                    //pixCfg[el.key()]["Data"][col].push_back(tmp);
                    pixCfg[el.key()]["Data"][col][line] = tmp;
                    col++;
                }
                colnum = col;
            }
            else {
                int col = line;
                int row = 0;
                for (auto tmp: el.value()) {
                    //pixCfg[el.key()]["Data"][col].push_back(tmp);
                    pixCfg[el.key()]["Data"][col][row] = tmp;
                    row++;
                }
                rownum = row;
            }
        }
        line++;
        if (lineRow) rownum = j[chip_type]["PixelConfig"].size();
        else colnum = j[chip_type]["PixelConfig"].size();
    }

    json chipCfg;
    chipCfg["PixelConfig"] = pixCfg;

    chipCfg["Type"] = chip_type;
    chipCfg["Name"] = chip_name;
    chipCfg["is_null"] = false;

    return chipCfg;
}

/// check dat file
json readDataFile(std::string filepath, std::string chip_name) {
#ifdef DEBUG
    std::cout << "readDataFile : " << filepath << " chipName : " << chip_name << std::endl;
#endif

    json data_json = {{ "is_null", true }};

    struct stat filestat;
    if (stat(filepath.c_str(), &filestat)) return data_json; //skip if file is invalid
    if (S_ISDIR(filestat.st_mode)) return data_json; //skip if file is a directory
    if (filepath.find(chip_name.c_str())==std::string::npos) return data_json; // skip if file doesn't have chip name
    if (
        filepath.find_last_of(".")==std::string::npos &&
        filepath.substr(filepath.find_last_of("."))!=".dat" &&
        filepath.substr(filepath.find_last_of("."))!=".json"
    ) return data_json; // skip if file is not data file


    if (filepath.substr(filepath.find_last_of("."))==".dat") {
        data_json = datToJson(filepath);
    } else if (filepath.substr(filepath.find_last_of("."))==".json") {
        data_json = fileToJson(filepath);
    }

    if (data_json["Type"].empty()||data_json["Name"].empty()) {
        data_json["is_null"] = true;
        return data_json;
    }

    data_json["is_null"] = false;

    return data_json;
}
