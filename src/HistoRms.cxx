#include "HistoRms.h"

///////////////////////////////////////////////////////////////////////////
/// Class for RMS histogram
HistoRms::HistoRms() :
Histo1D()
{
#ifdef DEBUG
    std::cout << "HistoRms::HistoRms()" << std::endl;
#endif
};
HistoRms::~HistoRms() {
#ifdef DEBUG
    std::cout << "HistoRms::~HistoRms()" << std::endl;
#endif
};

double HistoRms::getMean() {
#ifdef DEBUG
    std::cout << "HistoRms::getMean()" << std::endl;
#endif
    return m_mean;
}

double HistoRms::getRms() {
#ifdef DEBUG
    std::cout << "HistoRms::getRms()" << std::endl;
#endif
    return m_rms;
}

void HistoRms::setParameters(json& j) {
#ifdef DEBUG
    std::cout << "HistoRms::setParameters(json)" << std::endl;
#endif
    Histo1D::setParameters(j);
}
void HistoRms::setParameters(TH1* h) {
#ifdef DEBUG
    std::cout << "HistoRms::setParameters(TH1)" << std::endl;
#endif
    Histo1D::setParameters(h);
}
void HistoRms::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoRms::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo1d";

    m_axis.histo.x.nbins = 6;
    m_axis.histo.x.low   = 0;
    m_axis.histo.x.high  = 6;

    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = 0;
    m_axis.histo.y.high  = 1;

    m_axis.histo.x.title = "Deviation from the Mean [RMS] ";
    m_axis.histo.y.title = "Number of Pixels";
    m_axis.histo.z.title = "";
}

void HistoRms::fillHisto(json& j) {
#ifdef DEBUG
    std::cout << "HistoRms::fillHisto(j)" << std::endl;
#endif
    m_mean = m_h1->GetMean();
    m_rms = m_h1->GetRMS();
    int nbinsx = m_axis.data.x.nbins;
    int nbinsy = m_axis.data.y.nbins;
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = j["Data"][col][row];
            int bin_rms = whichSigma(tmp, m_mean, m_rms, 1, 6);
            //m_h->AddBinContent(bin_rms);
            m_h->Fill(bin_rms-1);
        }
    }
};
void HistoRms::fillHisto(TH1* h) {
#ifdef DEBUG
    std::cout << "HistoRms::fillHisto(TH1)" << std::endl;
#endif
    Histo1D::fillHisto(h);
};
void HistoRms::fillHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "HistoRms::fillHisto(TH2)" << std::endl;
#endif
    m_mean = m_h1->GetMean();
    m_rms = m_h1->GetRMS();
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    for (int row=0; row<nbinsy; row++) {
        for (int col=0; col<nbinsx; col++) {
            double tmp = h->GetBinContent(col+1, row+1);
            int bin_rms = whichSigma(tmp, m_mean, m_rms, 1, 6);
            //m_h->AddBinContent(bin_rms);
            m_h->Fill(bin_rms-1);
        }
    }
};

void HistoRms::drawHisto() {
#ifdef DEBUG
    std::cout << "HistoRms::drawHisto()" << std::endl;
#endif
    Histo1D::drawHisto();

    const char *LabelName[6] = {"1","2","3","4","5",">5"};
    for (int i=1; i<=6; i++) m_h->GetXaxis()->SetBinLabel(i, LabelName[i-1]);
    m_h->GetXaxis()->LabelsOption("h");
    gStyle->SetPaintTextFormat(".0f");
    m_h->SetMarkerSize(1.8);
    m_h->SetMarkerColor(1);
    m_h->Draw("TEXT0 SAME");
    m_h->GetYaxis()->SetRangeUser(0,((m_h->GetBinContent(m_h->GetMaximumBin()))*1.25));
    m_c->Update();
};

json HistoRms::getResult() {
#ifdef DEBUG
    std::cout << "HistoRms::getResult()" << std::endl;
#endif
    json result = Histo1D::getResult();
    result["tuned"] = m_h->GetBinContent(1)+m_h->GetBinContent(2)+m_h->GetBinContent(3);
    result["mean"] = m_mean;
    result["rms"] = m_rms;

    return result;
}
