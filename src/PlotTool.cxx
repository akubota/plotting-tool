#include "PlotTool.h"

///////////////////////////////////////////////////////////////////////////
/// Class for plotting
PlotTool::PlotTool():
m_h(nullptr), m_j(nullptr), m_h1(nullptr), m_h2(nullptr), m_f(nullptr), m_c( nullptr ),
m_fe_name(""), m_type(""), m_data_name("data"), m_chip_type("Asic"), m_chip_name("JohnDoe"),
m_data_format(-1), m_thr(0)
{
#ifdef DEBUG
    std::cout << "PlotTool::PlotTool()" << std::endl;
#endif
};

PlotTool::~PlotTool() {
#ifdef DEBUG
    std::cout << "PlotTool::~PlotTool()" << std::endl;
#endif
    delete m_h;
    delete m_h1;
    delete m_h2;
    delete m_f;
};

void PlotTool::build() {
#ifdef DEBUG
    std::cout << "PlotTool::build()" << std::endl;
#endif
    if (m_type=="") m_histo_name = m_chip_name + "_" + m_data_name;
    else            m_histo_name = m_chip_name + "_" + m_data_name + "_" + m_type;
    std::replace(m_histo_name.begin(), m_histo_name.end(), '-', '_');

    this->buildHisto();
    if      (m_data_format==0) this->fillHisto(m_j);
    else if (m_data_format==1) this->fillHisto((TH1*)m_h1);
    else if (m_data_format==2) this->fillHisto((TH2*)m_h2);
}

void PlotTool::write() {
#ifdef DEBUG
    std::cout << "PlotTool::write()" << std::endl;
#endif
    m_h->Write();
}
void PlotTool::write(TH1* h) {
#ifdef DEBUG
    std::cout << "PlotTool::write(TH1)" << std::endl;
#endif
    m_h = (TH1*)h->Clone();
    //this->setParameters(m_h);
    this->write();
}
void PlotTool::write(TH2* h) {
#ifdef DEBUG
    std::cout << "PlotTool::write(TH2)" << std::endl;
#endif
    m_h = (TH2*)h->Clone();
    //this->setParameters((TH2*)m_h);
    this->write();
}

void PlotTool::print(std::string dir, std::string ext) {
#ifdef DEBUG
    std::cout << "PlotTool::print("<<dir<<", " << ext << ")" << std::endl;
#endif
    this->setCanvas();
    this->drawHisto();
    this->printHisto(dir, ext);
}

void PlotTool::setParameters(json& data) {
#ifdef DEBUG
    std::cout << "PlotTool::setParameters(data)" << std::endl;
#endif
    if (!data["Type"].is_null()) m_histo_type = data["Type"];

    if (!data["x"]["Bins"].is_null()) m_axis.histo.x.nbins = data["x"]["Bins"];
    if (!data["x"]["Low"].is_null())  m_axis.histo.x.low   = data["x"]["Low"];
    if (!data["x"]["High"].is_null()) m_axis.histo.x.high  = data["x"]["High"];

    if (!data["y"]["Bins"].is_null()) m_axis.histo.y.nbins = data["y"]["Bins"];
    if (!data["y"]["Low"].is_null())  m_axis.histo.y.low   = data["y"]["Low"];
    if (!data["y"]["High"].is_null()) m_axis.histo.y.high  = data["y"]["High"];

    if (!data["z"]["Bins"].is_null()) m_axis.histo.z.nbins = data["z"]["Bins"];
    if (!data["z"]["Low"].is_null())  m_axis.histo.z.low   = data["z"]["Low"];
    if (!data["z"]["High"].is_null()) m_axis.histo.z.high  = data["z"]["High"];

    if (!data["x"]["AxisTitle"].is_null()) m_axis.histo.x.title = data["x"]["AxisTitle"];
    if (!data["y"]["AxisTitle"].is_null()) m_axis.histo.y.title = data["y"]["AxisTitle"];
    if (!data["z"]["AxisTitle"].is_null()) m_axis.histo.z.title = data["z"]["AxisTitle"];

    for (auto i : data["x"]["AxisLabel"]) {
        m_axis.histo.x.label.push_back(i);
    }
    for (auto i : data["y"]["AxisLabel"]) {
        m_axis.histo.y.label.push_back(i);
    }

    m_axis.overwrite = false;
    if (!data["Overwrite"].is_null()) m_axis.overwrite = data["Overwrite"];
};
void PlotTool::setParameters(TH1* h) {
#ifdef DEBUG
    std::cout << "PlotTool::setParameters(TH1)" << std::endl;
#endif
    m_histo_type = "Histo1d";

    m_axis.histo.x.nbins = h->GetNbinsX();
    m_axis.histo.x.low   = h->GetXaxis()->GetXmin();
    m_axis.histo.x.high  = h->GetXaxis()->GetXmax();

    m_axis.histo.y.nbins = 1;
    m_axis.histo.y.low   = h->GetBinContent(h->GetMinimumBin());
    m_axis.histo.y.high  = h->GetBinContent(h->GetMaximumBin());

    m_axis.histo.x.title = h->GetXaxis()->GetTitle();
    m_axis.histo.y.title = h->GetYaxis()->GetTitle();
    m_axis.histo.z.title = h->GetZaxis()->GetTitle();

    if (h->GetXaxis()->GetLabels()) {
        for (int i=0; i<m_axis.histo.x.nbins; i++) {
            m_axis.histo.x.label.push_back(h->GetXaxis()->GetBinLabel(i+1));
        }
    }

    m_axis.overwrite = false;
};
void PlotTool::setParameters(TH2* h) {
#ifdef DEBUG
    std::cout << "PlotTool::setParameters(TH2)" << std::endl;
#endif
    m_histo_type = "Histo2d";

    m_axis.histo.x.nbins = h->GetNbinsX();
    m_axis.histo.x.low   = h->GetXaxis()->GetXmin();
    m_axis.histo.x.high  = h->GetXaxis()->GetXmax();

    m_axis.histo.y.nbins = h->GetNbinsY();
    m_axis.histo.y.low   = h->GetYaxis()->GetXmin();
    m_axis.histo.y.high  = h->GetYaxis()->GetXmax();

    m_axis.histo.z.nbins = 1;
    m_axis.histo.z.low   = 0;
    m_axis.histo.z.high  = h->GetBinContent(h->GetMaximumBin());

    m_axis.histo.x.title = h->GetXaxis()->GetTitle();
    m_axis.histo.y.title = h->GetYaxis()->GetTitle();
    m_axis.histo.z.title = h->GetZaxis()->GetTitle();

    if (h->GetXaxis()->GetLabels()) {
        for (int i=0; i<m_axis.histo.x.nbins; i++) {
            m_axis.histo.x.label.push_back(h->GetXaxis()->GetBinLabel(i+1));
        }
    }
    if (h->GetYaxis()->GetLabels()) {
        for (int i=0; i<m_axis.histo.y.nbins; i++) {
            m_axis.histo.y.label.push_back(h->GetYaxis()->GetBinLabel(i+1));
        }
    }

    m_axis.overwrite = false;
};

json PlotTool::getParameters() {
#ifdef DEBUG
    std::cout << "PlotTool::getParameters()" << std::endl;
#endif
    json data;

    data["Type"] = m_histo_type;

    data["x"]["Bins"] = m_h->GetNbinsX();
    data["x"]["Low"]  = m_h->GetXaxis()->GetXmin();
    data["x"]["High"] = m_h->GetXaxis()->GetXmax();

    if (m_histo_type=="Histo1d") {
        data["y"]["Bins"] = 1;
        data["y"]["Low"]  = m_h->GetBinContent(m_h->GetMinimumBin());
        data["y"]["High"] = m_h->GetBinContent(m_h->GetMaximumBin());
    } else {
        data["y"]["Bins"] = m_h->GetNbinsY();
        data["y"]["Low"]  = m_h->GetYaxis()->GetXmin();
        data["y"]["High"] = m_h->GetYaxis()->GetXmax();

        data["z"]["Bins"] = 1;
        data["z"]["Low"]  = m_h->GetBinContent(m_h->GetMinimumBin());
        data["z"]["High"] = m_h->GetBinContent(m_h->GetMaximumBin());
    }

    data["x"]["AxisTitle"] = m_h->GetXaxis()->GetTitle();
    data["y"]["AxisTitle"] = m_h->GetYaxis()->GetTitle();
    data["z"]["AxisTitle"] = m_h->GetZaxis()->GetTitle();

    if (m_h->GetXaxis()->GetLabels()) {
        for (int i=0; i<m_h->GetNbinsX(); i++) {
            data["x"]["AxisLabel"].push_back(m_h->GetXaxis()->GetBinLabel(i+1));
        }
    }
    if (m_h->GetYaxis()->GetLabels()) {
        for (int i=0; i<m_h->GetNbinsY(); i++) {
            data["y"]["AxisLabel"].push_back(m_h->GetYaxis()->GetBinLabel(i+1));
        }
    }

    data["Overwrite"] = m_axis.overwrite;

    return data;
};

void PlotTool::setThreshold(double thr) {
#ifdef DEBUG
    std::cout << "PlotTool::setThreshold(" << thr << ")" << std::endl;
#endif
    m_thr = thr;
}
//void PlotTool::setFileName(std::string i_plot_name, std::string i_fe_name="", std::string i_type="") {
//#ifdef DEBUG
//    std::cout << "PlotTool::setFileName(" << i_plot_name << ", " << i_fe_name << ", " << i_type << ")" << std::endl;
//#endif
////    m_filename = i_plot_name;
////
////    if ( strstr( m_filename.c_str(), "NoiseOccupancy")!=nullptr )    m_data_type = "noise_occ";
////    else if ( strstr( m_filename.c_str(), "OccupancyMap")!=nullptr ) m_data_type = "occ";
////    else if ( strstr( m_filename.c_str(), "MeanTotMap")!=nullptr )   m_data_type = "mean_tot";
////    else if ( strstr( m_filename.c_str(), "SigmaTotMap")!=nullptr )  m_data_type = "sigma_tot";
////    else if ( strstr( m_filename.c_str(), "ThresholdMap")!=nullptr ) m_data_type = "thr";
////    else if ( strstr( m_filename.c_str(), "NoiseMap")!=nullptr)      m_data_type = "noi";
////    else                                                          m_data_type = "other";
//};

void PlotTool::setHisto(TH1* h) {
#ifdef DEBUG
    std::cout << "PlotTool::setHisto(h)" << std::endl;
#endif
    m_h1 = h;
};
void PlotTool::setHisto(TH2* h) {
#ifdef DEBUG
    std::cout << "PlotTool::setHisto(h)" << std::endl;
#endif
    m_h2 = h;
};
void PlotTool::setHisto(TF1* f) {
#ifdef DEBUG
    std::cout << "PlotTool::setHisto(f)" << std::endl;
#endif
    m_f = f;
};

TH1* PlotTool::getTH() {
#ifdef DEBUG
    std::cout << "PlotTool::getTH()" << std::endl;
#endif
    return (TH1*)m_h->Clone();
}
TF1* PlotTool::getTF() {
#ifdef DEBUG
    std::cout << "PlotTool::getTF()" << std::endl;
#endif
    return (TF1*)m_f->Clone();
}

void PlotTool::printHisto(std::string dir, std::string ext) {
#ifdef DEBUG
    std::cout << "PlotTool::printHisto(" << dir << ", " << ext << ")" << std::endl;
#endif
    TLatex *tname= new TLatex();
    style_TLatex(tname, 22, 73, 30);
    std::string latex;

    /// Write Chip Type on the Canvas
    latex = m_chip_type + " Internal";
    tname->DrawLatex(m_latex_pos[0], m_latex_pos[1], latex.c_str());

    /// Write Chip Name on the Canvas
    latex = "Chip SN: " + m_chip_name;
    tname->DrawLatex(m_latex_pos[2], m_latex_pos[3], latex.c_str());

    this->setText();
    m_c->Update();

    std::string title = dir + "/" + m_h->GetName() + "_root." + ext;
    m_c->Print(title.c_str());
    delete m_c;
};

void PlotTool::setText() {
#ifdef DEBUG
    std::cout << "PlotTool::setText()" << std::endl;
#endif
    /// Write the Gaussian mean/sigma and the histogram mean/rms on the Canvas.
    TLatex *tlatex = new TLatex;
    style_TLatex(tlatex, 13, 63, 24);
    double pos_x, pos_y;
    std::vector <std::string> latexs;

    // left
    pos_x = 0.18;
    pos_y = 0.85;
    latexs = this->getTLatexLeft();
    for (auto text : latexs) {
        double p_x = pos_x;
        double p_y = pos_y;
        tlatex->DrawLatex(p_x, p_y, text.c_str());
        pos_y = pos_y-0.05;
    }

    // right
    pos_x = 0.63;
    pos_y = 0.85;
    //if (m_fe_name!="") {
    //    TLegend* l = new TLegend(p[0], p[1], p[2], p[3]);
    //    l->SetHeader("Analog FEs", "C");
    //    l->AddEntry(m_h, m_fe_name.c_str(), "f");
    //    l->SetBorderSize(0);
    //    l->Draw();
    //    pos_y = 0.81;
    //}
    latexs = this->getTLatexRight();
    for (auto text : latexs) {
        double p_x = pos_x;
        double p_y = pos_y;
        tlatex->DrawLatex(p_x, p_y, text.c_str());
        pos_y = pos_y-0.05;
    }
};
void PlotTool::setTLatexLeft(std::string i_text) {
#ifdef DEBUG
    std::cout << "PlotTool::setTLatexLeft(" << i_text << ")" << std::endl;
#endif
    m_latex_left.push_back(i_text);
};
void PlotTool::setTLatexRight(std::string i_text) {
#ifdef DEBUG
    std::cout << "PlotTool::setTLatexRight(" << i_text << ")" << std::endl;
#endif
    m_latex_right.push_back(i_text);
};
std::vector <std::string> PlotTool::getTLatexLeft() {
#ifdef DEBUG
    std::cout << "PlotTool::getTLatexLeft()" << std::endl;
#endif
    return m_latex_left;
};
std::vector <std::string> PlotTool::getTLatexRight() {
#ifdef DEBUG
    std::cout << "PlotTool::getTLatexRight()" << std::endl;
#endif
    return m_latex_right;
};

int PlotTool::whichSigma(double value, double mean, double sigma, double bin_width, int bin_num) {
#ifdef DEBUG
    std::cout << "PlotTool::whichSigma" << std::endl;
#endif
    int hist_bin = -1;
    for (int i=1; i<bin_num+1; i++) {
            if ( fabs(mean-value) >= ((i-1)*bin_width*sigma) && fabs(mean-value) < (i*bin_width*sigma) ) hist_bin=i;
    }

    if ( fabs(mean-value) >= (bin_num*bin_width*sigma) ) hist_bin=bin_num;
    else if ( fabs(mean-value) < (bin_width*sigma) ) hist_bin=1;

    return hist_bin;
}

int PlotTool::whichBin(double value, double occnum) {
#ifdef DEBUG
    std::cout << "PlotTool::whichBin()" << std::endl;
#endif
    int hist_bin = -1;
    double zero = 0.0;
    double ninety8 = value * 0.98;
    double hundred02 = value * 1.02;
    if (occnum == zero) hist_bin = 1;
    else if (occnum > zero && occnum < ninety8) hist_bin = 2;
    else if (occnum >= ninety8 && occnum < value) hist_bin = 3;
    else if (occnum == value) hist_bin = 4;
    else if (occnum > value && occnum <= hundred02) hist_bin = 5;
    else if (occnum > hundred02) hist_bin = 6;

    return hist_bin;
}

int PlotTool::whichFE(int row, int col) {
#ifdef DEBUG
    std::cout << "PlotTool::whichFE" << std::endl;
#endif
    int which_fe = 10; //add if 10, give error in .cxx file
    if (m_chip_type=="RD53A") {
        int division1 = 128, division2 = 264;
        if (col<division1){ //Syn FE
            which_fe = 0;
        } else if (col>=division1 && col<division2){ //Lin FE
            which_fe = 1;
        } else if (col>=division2) { //Diff FE
            which_fe = 2;
        }
    } else if (m_chip_type=="FE-I4B") {
        which_fe = 0;
    }
    return which_fe;
}

//Style for TH1
void PlotTool::style_TH1(TH1* h, const char* Xtitle, const char* Ytitle, int color){
#ifdef DEBUG
    std::cout << "PlotTool::style_TH1()" << std::endl;
#endif
    h->SetStats(0);
    h->SetTitle(0);
    h->GetXaxis()->SetTitle(Xtitle);
    h->GetYaxis()->SetTitle(Ytitle);
    h->GetXaxis()->SetTitleSize(0.045);
    h->GetXaxis()->SetTitleOffset(1.25);
    h->GetYaxis()->SetTitleSize(0.045);
    h->GetYaxis()->SetTitleOffset(1.7);
    h->GetXaxis()->SetLabelSize(0.045);
    h->GetYaxis()->SetLabelSize(0.045);
    h->SetFillColor(color);
    h->SetLineColor(color);
}

//Style for TGraph
void PlotTool::style_TGraph(TGraph* h, const char* Xtitle, const char* Ytitle, int color){
#ifdef DEBUG
    std::cout << "PlotTool::style_TGraph()" << std::endl;
#endif
    h->SetTitle(0);
    h->GetXaxis()->SetTitle(Xtitle);
    h->GetYaxis()->SetTitle(Ytitle);
    h->GetXaxis()->SetTitleSize(0.045);
    h->GetXaxis()->SetTitleOffset(1.25);
    h->GetYaxis()->SetTitleSize(0.045);
    h->GetYaxis()->SetTitleOffset(1.7);
    h->GetXaxis()->SetLabelSize(0.045);
    h->GetYaxis()->SetLabelSize(0.045);
    h->SetMarkerColor(color);
    h->SetLineColor(color);
}

//Style for THStack (TH1)
void PlotTool::style_THStack(THStack* h, const char* Xtitle, const char*Ytitle){
#ifdef DEBUG
    std::cout << "PlotTool::style_THStack()" << std::endl;
#endif
    h->SetTitle(0);
    h->GetXaxis()->SetTitle(Xtitle);
    h->GetYaxis()->SetTitle(Ytitle);
    h->GetXaxis()->SetTitleSize(0.045);
    h->GetXaxis()->SetTitleOffset(1.25);
    h->GetYaxis()->SetTitleSize(0.045);
    h->GetYaxis()->SetTitleOffset(1.7);
    h->GetXaxis()->SetLabelSize(0.045);
    h->GetYaxis()->SetLabelSize(0.045);
}

//Style for TH2
void PlotTool::style_TH2(TH2* h, const char* Xtitle, const char*Ytitle, const char* Ztitle, int color){
#ifdef DEBUG
    std::cout << "PlotTool::style_TH2()" << std::endl;
#endif
    h->SetStats(0);
    h->SetTitle(0);
    h->GetXaxis()->SetTitle(Xtitle);
    h->GetYaxis()->SetTitle(Ytitle);
    h->GetZaxis()->SetTitle(Ztitle);
    h->GetXaxis()->SetTitleSize(0.045);
    h->GetXaxis()->SetTitleOffset(1.25);
    h->GetYaxis()->SetTitleSize(0.045);
    h->GetYaxis()->SetTitleOffset(1.7);
    h->GetZaxis()->SetTitleSize(0.045);
    h->GetZaxis()->SetTitleOffset(1.3);
    h->GetXaxis()->SetLabelSize(0.045);
    h->GetYaxis()->SetLabelSize(0.045);
    h->GetZaxis()->SetLabelSize(0.045);
    gStyle->SetPalette(color);
}

//Style for TH1 Canvas
void PlotTool::style_TH1canvas(TCanvas *c){
    c->SetTopMargin(0.12);
    c->SetLeftMargin(0.15);
    c->SetRightMargin(0.05);
    c->SetBottomMargin(0.1225);
}

//Style for TH2 Canvas
void PlotTool::style_TH2canvas(TCanvas *c){
    c->SetTopMargin(0.12);
    c->SetLeftMargin(0.15);
    c->SetRightMargin(0.18);
    c->SetBottomMargin(0.1225);
}

//Style for TLatex --> RD53A and Chip Id labels.
void PlotTool::style_TLatex(TLatex* name, int align, int font, int size){
    name->SetNDC();
    name->SetTextAlign(align);
    name->SetTextFont(font);
    name->SetTextSizePixels(size);
}

void PlotTool::setChip(std::string i_type, std::string i_name, std::string i_fe_name) {
#ifdef DEBUG
    std::cout << "PlotTool::setChip(" << i_type << ", " << i_name << ", " << i_fe_name << ")" << std::endl;
#endif
    m_chip_type = i_type;
    m_chip_name = i_name;
    m_fe_name = i_fe_name;
};

void PlotTool::setData(json& j, std::string i_name, std::string i_type) {
#ifdef DEBUG
    std::cout << "PlotTool::setData(json, " << i_name << ", " << i_type << ")" << std::endl;
#endif
    m_j           = j;
    m_data_name   = i_name;
    m_type        = i_type;
    m_data_format = 0;
    this->setParameters(m_j);
};
void PlotTool::setData(TH1* h, std::string i_name, std::string i_type) {
#ifdef DEBUG
    std::cout << "PlotTool::setData(TH1, " << i_name << ", " << i_type << ")" << std::endl;
#endif
    m_h1          = (TH1*)h->Clone(Form("%s_clone",h->GetName()));
    m_data_name   = i_name;
    m_type        = i_type;
    m_data_format = 1;
    this->setParameters(m_h1);
};
void PlotTool::setData(TH2* h, std::string i_name, std::string i_type) {
#ifdef DEBUG
    std::cout << "PlotTool::setData(TH2, " << i_name << ", " << i_type << ")" << std::endl;
#endif
    m_h2          = (TH2*)h->Clone(Form("%s_clone",h->GetName()));
    m_data_name   = i_name;
    m_type        = i_type;
    m_data_format = 2;
    this->setParameters(m_h2);
};

std::string PlotTool::getChipName() {
#ifdef DEBUG
    std::cout << "PlotTool::getChipName() { return " << m_chip_name << "; }" << std::endl;
#endif
    return m_chip_name;
};
