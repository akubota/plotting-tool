#include "Process.h"
#include "Env.h"

///////////////////////////////////////////////////////////////////////////

////////////////////////////////////////
/// Make ROOT file from result directory
int dirToRoot(std::string i_dir, std::string i_file) {
#ifdef DEBUG
    std::cout << "dirToRoot(" << i_dir << ", " << i_file << ")" << std::endl;
#endif
    std::string filetype, filepath;
    Env* e { nullptr };

    //////////////////
    /// Open Directory
    DIR *dp { nullptr };
    struct dirent *dirp { nullptr };
    dp = opendir(i_dir.c_str());
    if ( !dp ) {
        std::cerr << "Directory not found: " << i_dir << std::endl;
        return -1;
    }

    //////////////
    /// Open TFile
    TFile* rootfile = new TFile(i_file.c_str(), "RECREATE");

    ///////////
    /// dataLog
    json dataLog;
    dataLog["chips"] = json::array();

    ///////////////////
    /// scanLog to TEnv
    filetype = "scanLog";
    filepath = i_dir + "/scanLog.json";
    e = new Env(filetype, filepath);
    json scanLog = e->getJson();
    e->write();
    delete e;

    std::string test_type = scanLog["testType"];
    std::string chip_type = scanLog["chipType"];

    ///////////////////////
    /// scan config to TEnv
    filetype = "scanCfg";
    filepath = i_dir + "/" + test_type + ".json";
    struct stat filestat;
    if (!stat(filepath.c_str(), &filestat)) {
        e = new Env(filetype, filepath);
        json scanCfg = e->getJson();
        e->write();
        delete e;
    }

    ////////////////
    /// chip configs
    int chip_nums = scanLog["chips"].size();
    for (int i=0; i<chip_nums; i++) {
        int chip_num = i;
        json chipLog;
        json chip_j = scanLog["chips"][chip_num];
        std::string chip_name = "DisabledChip_" + std::to_string(chip_num);

        /// config files
        if (!chip_j["enable"].is_null()&&chip_j["enable"]==0) {
            chipLog["Name"] = chip_name;
        } else {
            std::string cfg_path = chip_j["config"];

            /// before config
            filetype = "beforeCfg";
            filepath = i_dir + "/" + cfg_path + ".before";
            saveConfig(filepath, chip_type, filetype, chipLog);

            /// after config
            filetype = "afterCfg";
            filepath = i_dir + "/" + cfg_path + ".after";
            saveConfig(filepath, chip_type, filetype, chipLog);
        }
        dataLog["chips"].push_back(chipLog);
    }

    ////////////////////
    /// chip output data
    while ((dirp = readdir(dp))) {
        filepath = i_dir + "/" + dirp->d_name;
        std::string chip_name = "";
        int chip_num = 0;
        json plotData;
        for (int i=0; i<chip_nums; i++) {
            chip_num = i;
            chip_name = dataLog["chips"][chip_num]["Name"];
            plotData = readDataFile(filepath, chip_name);
            if (!plotData["is_null"]) break;
        }
        if (plotData["Type"].is_null()||plotData["Name"].is_null()) continue;
        saveData( chip_name, chip_type, plotData );
        std::string name = plotData["Name"];
        dataLog["chips"][chip_num]["data"][name] = plotData["Type"];
    }

    ///////////////////
    /// dataLog to TEnv
    filetype = "dataLog";
    e = new Env(filetype, dataLog);
    e->write();
    delete e;

    ///////////////
    /// Close TFile
    rootfile->Close();

    return 0;
}

/////////////////////////////
/// save data file to TH1/TH2
void saveData(std::string chip_name, std::string chip_type, json& data) {
#ifdef DEBUG
    std::cout << "saveData(json)" << std::endl;
#endif
    std::string hist_type = data["Type"];
    std::string name = data["Name"];

    PlotTool* pH;
    if      (hist_type=="Histo1d") pH = new Histo1D();
    else if (hist_type=="Histo2d") pH = new Histo2D();
    else                           return;

    pH->setChip(chip_type, chip_name);
    pH->setData(data, name);
    pH->build();
    pH->write();
    delete pH;
}

/////////////////////////////
/// save data file to TH1/TH2
void saveData(std::string chip_name, std::string chip_type, std::string data_type, std::string data_name, std::string o_dir, TH1* h, bool print, std::string ext) {
#ifdef DEBUG
    std::cout << "saveData(TH)" << std::endl;
#endif
    std::string key = chip_name+"_"+data_name;
    std::replace(key.begin(), key.end(), '-', '_');

    PlotTool* pH;
    if      (data_type=="Histo1d") pH = new Histo1D();
    else if (data_type=="Histo2d") pH = new Histo2D();
    else                           return;

    pH->setChip(chip_type, chip_name);
    if      (data_type=="Histo1d") {
        pH->setParameters((TH1*)h);
        pH->write((TH1*)h);
    }
    else if (data_type=="Histo2d") {
        pH->setParameters((TH2*)h);
        pH->write((TH2*)h);
    }

    if (print) pH->print(o_dir, ext);

    delete pH;
}

////////////////////////////////////
/// save chip config ile to TH2/TEnv
void saveConfig(std::string filepath, std::string chip_type, std::string filetype, json& log) {
#ifdef DEBUG
    std::cout << "saveConfig()" << std::endl;
#endif
    json chipCfg = readChipPixCfg(filepath, chip_type, filetype);
    if (!chipCfg["is_null"]) {
        //////////////////
        // pixelCfg to TH2
        std::string chip_name = chipCfg["Name"];
        for (auto& data : chipCfg["PixelConfig"].items()) {
            json plotData =  chipCfg["PixelConfig"][data.key()];
            if ( plotData["Name"].is_null()||plotData["Type"].is_null()) continue;
            saveData( chip_name, chip_type, plotData );
            std::string name = plotData["Name"];
            log["cfg"][name] = plotData["Type"];
        }
        ////////////////////
        // globalCfg to TEnv
        Env* e = new Env(filetype, filepath);
        e->write(chip_name+"_"+filetype);
        delete e;
        log["Name"] = chip_name;
    }
}

/////////////////////////////////
/// Make ROOT file from ROOT file
int analyzeRootFile(std::string i_file, std::string o_file, std::string o_dir, bool print, std::string ext) {
#ifdef DEBUG
    std::cout << "analyzeRootFile()" << std::endl;
#endif
    std::string filetype, filepath;
    Env* e { nullptr };

    //////////////
    /// Open TFile
    TFile* i_rootfile = new TFile(i_file.c_str());
    TFile* o_rootfile = new TFile(o_file.c_str(), "RECREATE");

    ///////////////////
    /// scanLog to json
    filetype = "scanLog";
    e = new Env(filetype, (TEnv*)i_rootfile->Get("scanLog"));
    json scanLog = e->getJson();
    o_rootfile->cd();
    e->write();
    delete e;

    std::string chip_type = scanLog["chipType"];

    ///////////////////////
    /// scan config to TEnv
    filetype = "scanCfg";
    if (i_rootfile->FindKey("scanCfg")) {
        e = new Env(filetype, (TEnv*)i_rootfile->Get("scanCfg"));
        o_rootfile->cd();
        e->write();
        delete e;
    }

    ///////////////////
    /// dataLog to json
    filetype = "dataLog";
    e = new Env(filetype, (TEnv*)i_rootfile->Get("dataLog"));
    json dataLog = e->getJson();

    std::string key, data_name, data_type, chip_name;
    int chip_nums = dataLog["chips"].size();
    for (int i=0; i<chip_nums; i++) {
        int chip_num = i;
        json chip_data = dataLog["chips"][chip_num];
        chip_name = chip_data["Name"];
        ////////////////////
        /// chip config data
        for (auto& data : chip_data["cfg"].items()) {
            data_name = data.key();
            data_type = data.value();

            key = chip_name+"_"+data_name;
            std::replace(key.begin(), key.end(), '-', '_');
            TH1* h = (TH1*)i_rootfile->Get(key.c_str());

            o_rootfile->cd();
            saveData(chip_name, chip_type, data_type, data_name, o_dir, h, print, ext);
            dataLog["chips"][chip_num]["plot"][data_name] = data_type;

            std::string axisX = h->GetXaxis()->GetTitle();
            std::string axisY = h->GetYaxis()->GetTitle();

            if (axisX=="Column"&&axisY=="Row") {
                //////////////
                /// HistoProjection
                HistoProjection* pP = new HistoProjection();
                pP->setChip(chip_type, chip_name);
                pP->setData((TH2*)h, data_name, "projection");
                pP->build();
                pP->write();
                if (print) pP->print(o_dir, ext);
                dataLog["chips"][chip_num]["plot"][data_name+"_projection"] = "Histo1d";

                delete pP;
            }
        }
        ///////////////
        /// output data
        for (auto& data : chip_data["data"].items()) {
            data_name = data.key();
            data_type = data.value();

            key = chip_name+"_"+data_name;
            std::replace(key.begin(), key.end(), '-', '_');
            TH1* h = (TH1*)i_rootfile->Get(key.c_str());

            o_rootfile->cd();
            saveData(chip_name, chip_type, data_type, data_name, o_dir, h, print, ext);
            dataLog["chips"][chip_num]["plot"][data_name] = data_type;

            std::string axisX = h->GetXaxis()->GetTitle();
            std::string axisY = h->GetYaxis()->GetTitle();

            if ((axisX=="Column"||axisX=="Col")&&axisY=="Row") {
                //////////////
                /// HistoProjection
                HistoProjection* pP = new HistoProjection();
                pP->setChip(chip_type, chip_name);
                pP->setData((TH2*)h, data_name, "projection");
                pP->build();
                pP->write();

                /////////////
                /// Occupancy
                if ( data_name.find("OccupancyMap") != std::string::npos )
                {
                    HistoOccupancy* pO = new HistoOccupancy();
                    pO->setChip(chip_type, chip_name);
                    pO->setData((TH2*)h, data_name, "occupancy");
                    pO->build();
                    pO->write();
                    if (print) pO->print(o_dir, ext);
                    dataLog["chips"][chip_num]["plot"][data_name+"_occupancy"] = "Histo1d";
                    dataLog["chips"][chip_num]["ana"][data_name]["result"] = pO->getResult();
                    delete pO;

                    //dataLog["chips"][chip_num]["ana"][data_name]["gaus"] = pP->setRms();
                }

                ///////////////////
                /// Noise Occupancy
                if ( data_name.find("NoiseOccupancy") != std::string::npos )
                {
                    HistoNoiseOccupancy* pN = new HistoNoiseOccupancy();
                    pN->setChip(chip_type, chip_name);
                    pN->setData((TH2*)h, data_name, "noiseoccupancy");
                    pN->build();
                    pN->write();
                    if (print) pN->print(o_dir, ext);
                    dataLog["chips"][chip_num]["plot"][data_name+"_noiseoccupancy"] = "Histo1d";
                    dataLog["chips"][chip_num]["ana"][data_name]["result"] = pN->getResult();
                    delete pN;

                    //dataLog["chips"][chip_num]["ana"][data_name]["gaus"] = pP->setRms();
                }

                //////////////
                /// zero count
                if ( data_name.find("MeanTotMap")   != std::string::npos ||
                     data_name.find("SigmaTotMap")  != std::string::npos ||
                     data_name.find("ThresholdMap") != std::string::npos ||
                     data_name.find("NoiseMap")     != std::string::npos )
                {
                    dataLog["chips"][chip_num]["ana"][data_name]["zero"] = pP->setZero();
                }

                ///////
                /// RMS
                if ( data_name.find("MeanTotMap")   != std::string::npos ||
                     data_name.find("SigmaTotMap")  != std::string::npos ||
                     data_name.find("ThresholdMap") != std::string::npos ||
                     data_name.find("NoiseMap")     != std::string::npos )
                {
                    TH1* h_1 = pP->getTH();
                    HistoRms* pR = new HistoRms();
                    pR->setChip(chip_type, chip_name);
                    pR->setData((TH2*)h, data_name, "rms");
                    pR->setHisto((TH1*)h_1);
                    pR->build();
                    pR->write();
                    if (print) pR->print(o_dir, ext);
                    dataLog["chips"][chip_num]["plot"][data_name+"_rms"] = "Histo1d";
                    //dataLog["chips"][chip_num]["ana"][data_name]["gaus"] = pP->setRms(pR->getMean(), pR->getRms());
                    pP->setRms(pR->getMean(), pR->getRms());
                    dataLog["chips"][chip_num]["ana"][data_name]["result"] = pR->getResult();
                    delete pR;
                }

                ////////////
                /// Gaussian
                if ( data_name.find("ThresholdMap") != std::string::npos ||
                     data_name.find("NoiseMap")     != std::string::npos )
                {
                    TH1* h_1 = pP->getTH();
                    HistoGaus* pG = new HistoGaus();
                    pG->setChip(chip_type, chip_name);
                    pG->setData((TH2*)h, data_name, "gaus");
                    pG->fit(h_1);
                    pG->build();
                    pG->write();
                    if (print) pG->print(o_dir, ext);
                    dataLog["chips"][chip_num]["plot"][data_name+"_gaus"] = "Histo1d";
                    //pP->setPercent();
                    //dataLog["chips"][chip_num]["ana"][data_name]["gaus"] = pP->setGaus((TF1*)pG->getTF());
                    pP->setGaus((TF1*)pG->getTF());
                    dataLog["chips"][chip_num]["ana"][data_name]["result"] = pG->getResult();
                    delete pG;
                }

                if (print) pP->print(o_dir, ext);
                dataLog["chips"][chip_num]["plot"][data_name+"_projection"] = "Histo1d";

                delete pP;
            }

        }
    }
    o_rootfile->cd();

    ///////////////////
    /// dataLog to TEnv
    e->setData(dataLog);
    e->write();
    delete e;

    ///////////////
    /// Close TFile
    i_rootfile->Close();
    o_rootfile->Close();

    return 0;
}
