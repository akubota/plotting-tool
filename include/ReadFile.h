#ifndef __READFILE_H
#define __READFILE_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <TEnv.h>
#include <TROOT.h>
#include <THashList.h>

#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

/////////////////////////////////
/// convert dat file to json file
json datToJson(std::string /*filepath*/);

///////////////////
/// scanLog to json
json readScanLog(json& /*i_j*/);

///////////////////
/// chipCfg to json
json readChipGlobCfg(json& /*i_j*/);

////////////////
/// file to json
json fileToJson(std::string /*filepath*/, std::string filetype="file");

/// check chip config
json readChipPixCfg(std::string /*filepath*/,
                 std::string /*chip_type*/,
                 std::string /*type*/);

/// check dat file
json readDataFile(std::string /*filepath*/,
                 std::string /*chip_name*/);

#endif //__READFILE_H
