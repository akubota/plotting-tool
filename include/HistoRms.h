#ifndef __HISTORMS_H
#define __HISTORMS_H

#include "Histo1D.h"

class HistoRms : public Histo1D
{
public:
    HistoRms();
    ~HistoRms();

    double getMean();
    double getRms();
    json getResult();
private:
    void setParameters(json& /*j*/);
    void setParameters(TH1* /*h*/);
    void setParameters(TH2* /*h*/);

    /// Fill histogram from data
    void fillHisto(json& /*j*/);
    void fillHisto(TH1* /*h*/);
    void fillHisto(TH2* /*h*/);

    /// Draw histogram on the canvas
    virtual void drawHisto();

    double m_mean;
    double m_rms;
};

#endif //__RMS_H
