#ifndef __HISTOOCCUPANCY_H
#define __HISTOOCCUPANCY_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

#include "Histo1D.h"

class HistoOccupancy : public Histo1D
{
public:
    HistoOccupancy();
    ~HistoOccupancy();

    json getResult();
private:
    void setParameters(TH2* /*h*/);

    /// Fill histogram from data
    void fillHisto(json& /*j*/);
    void fillHisto(TH2* /*h*/);

    /// Draw histogram on the canvas
    void drawHisto();
};

#endif //__HISTOOCCUPANCY_H
