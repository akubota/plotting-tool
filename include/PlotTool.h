#ifndef __PLOTTOOL_H
#define __PLOTTOOL_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "TROOT.h"
#include <TStyle.h>
#include <TCanvas.h>
#include <TPaveStats.h>
#include <TH1.h>
#include <TF1.h>
#include <TH2.h>
#include <TMarker.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>

#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

class PlotTool
{
public:
    /// Deconstructor
    virtual ~PlotTool();

    /// Set parameters from data and config
    virtual void setParameters(json& /*data*/);
    virtual void setParameters(TH1* /*h*/);
    virtual void setParameters(TH2* /*h*/);

    json getParameters();

    void build();

    void write();
    void write(TH1* /*h*/);
    void write(TH2* /*h*/);

    void print(std::string /*dir*/, std::string /*ext*/);

    /// Set histogram
    void setHisto(TH1* /*h*/);
    void setHisto(TH2* /*h*/);
    void setHisto(TF1* /*f*/);

    TH1* getTH();
    TF1* getTF();

    int whichSigma(double /*value*/,
                   double /*mean*/,
                   double /*sigma*/,
                   double /*bin_width*/,
                   int /*bin_num*/);
    //Write a function where you give it occupancy and it returns a bin number
    int whichBin(double /*value*/,
                 double /*occnum*/);
    //Function for if pixel was in certain FE
    int whichFE(int /*row*/,
                int /*col*/);
    //Style for TH1
    void style_TH1(TH1* /*hist_TH1*/,
                   const char* /*Xtitle*/,
                   const char* /*Ytitle*/,
                   int color=1);
    //Style for TGraph
    void style_TGraph(TGraph* /*hist_TGraph*/,
                      const char* /*Xtitle*/,
                      const char* /*Ytitle*/,
                      int color=1);
    //Style for THStack (TH1)
    void style_THStack(THStack* /*hist_Stack*/,
                       const char* /*Xtitle*/,
                       const char* /*Ytitle*/);
    //Style for TH2
    void style_TH2(TH2* /*hist_TH2*/,
                   const char* /*Xtitle*/,
                   const char* /*Ytitle*/,
                   const char* /*Ztitle*/,
                   int color=107);

    //Style for TH1 Canvas
    void style_TH1canvas(TCanvas* /*c*/);
    //Style for TH2 Canvas
    void style_TH2canvas(TCanvas* /*c*/);
    //Style for TLatex --> RD53A and Chip Id labels.
    void style_TLatex(TLatex* /*name*/,
                      int /*align*/,
                      int /*font*/,
                      int /*size*/);


    /// Set chip name
    void setChip(std::string /*i_type*/, std::string /*i_name*/, std::string i_fe_name="");
    void setData(json& /*j*/, std::string /*i_name*/, std::string i_type="");
    void setData(TH1* /*h*/, std::string /*i_name*/, std::string i_type="");
    void setData(TH2* /*h*/, std::string /*i_name*/, std::string i_type="");

    /// Get chip name
    std::string getChipName();

    virtual json getResult() = 0;

    void setThreshold(double /*thr*/);

protected:
    /// Constructor
    PlotTool ();

    /// Build histogram
    virtual void buildHisto() = 0;

    /// Fill histogram from data
    virtual void fillHisto(json& /*j*/) = 0;
    virtual void fillHisto(TH1* /*h*/) = 0;
    virtual void fillHisto(TH2* /*h*/) = 0;

    /// Draw histogram on the canvas
    virtual void drawHisto() = 0;

    virtual void setCanvas() = 0;
    /// Print the canvas
    virtual void printHisto(std::string /*dir*/, std::string /*ext*/);

    /// Set text0 draw option
    void setText0(bool /*b*/);

    /// Set information on the canvas
    //void setText(double* /*p*/);
    void setText();

    /// Set TLatex text
    void setTLatexLeft(std::string /*i_text*/);
    void setTLatexRight(std::string /*i_text*/);

    /// Get TLatex text
    std::vector <std::string> getTLatexLeft();
    std::vector <std::string> getTLatexRight();

    TH1* m_h;

    json m_j;
    TH1* m_h1;
    TH2* m_h2;

    TF1* m_f;
    TCanvas* m_c;
    std::string m_filename;
    std::string m_fe_name;
    std::string m_type;
    std::string m_data_name;
    std::string m_data_type;
    std::vector <std::string> m_latex_left;
    std::vector <std::string> m_latex_right;
    std::vector <double> m_latex_pos;
    bool m_text0;
    typedef struct {
        typedef struct {
            int nbins;
            double low;
            double high;
            std::string title;
            std::vector <std::string> label;
        } Axis;
        typedef struct {
            Axis x;
            Axis y;
            Axis z;
            std::string type;
        } Histo;
        Histo histo;
        Histo data;
        bool overwrite;
    } PlotAxis;
    PlotAxis m_axis;

    std::string m_histo_name;
    std::string m_histo_type;
    std::string m_chip_type;
    std::string m_chip_name;
    std::string m_output_dir;
    std::string m_title;

    int m_data_format;
    double m_thr;
    int m_zeros;
    int m_occs;
    int m_nois;

private:
    //std::vector <int> m_color = {806, 824, 865, 800}; //(kOrange+6, kSpring+4, kAzure+5, kOrange)
};

#endif //__PLOTTOOL_H
