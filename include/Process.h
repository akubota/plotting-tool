#ifndef __PROCESS_H
#define __PROCESS_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <TFile.h>
#include <TStyle.h>
#include <TH1.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TMarker.h>
#include <TH2.h>
#include <TPaveStats.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TGraph.h>
#include <TEnv.h>
#include <TROOT.h>
#include <THashList.h>

#include <json.hpp>
#include "ReadFile.h"
#include "Histo1D.h"
#include "Histo2D.h"
#include "PlotTool.h"
#include "HistoProjection.h"
#include "HistoGaus.h"
#include "HistoRms.h"
#include "HistoOccupancy.h"
#include "HistoNoiseOccupancy.h"

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

int dirToRoot(std::string /*i_dir*/, std::string /*i_file*/);

void saveData(std::string /*chip_name*/, std::string /*chip_type*/, json& /*data*/);
void saveData(std::string /*chip_name*/, std::string /*chip_type*/, std::string /*data_type*/, std::string /*data_name*/, std::string /*o_dir*/, TH1* /*h*/, bool /*print*/, std::string /*ext*/);

void saveConfig(std::string /*filepath*/, std::string /*chip_type*/, std::string /*filetype*/, json& /*log*/); 

int analyzeRootFile(std::string /*i_file*/, std::string /*o_file*/, std::string /*o_dir*/, bool /*print*/, std::string /*ext*/);

void writeProjection(TH2* /*h*/, std::string /*name*/, std::string /*chip_name*/, std::string /*chip_type*/);


#endif //__PROCESS_H
