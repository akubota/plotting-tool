#ifndef __HISTO2D_H
#define __HISTO2D_H

#include <math.h> //for fabs()
#include <utility> //for pairs
#include <vector>
#include <algorithm> //for std::sort()
#include <iomanip>
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <json.hpp>
using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int32_t, std::uint32_t, float>;

#include "PlotTool.h"

class Histo2D : public PlotTool
{
public:
    /// Constructor
    Histo2D();

    /// Deconstructor
    ~Histo2D();

    virtual json getResult();

protected:
    virtual void buildHisto();
    virtual void setCanvas();
    virtual void drawHisto();

    virtual void setParameters(json& /*data*/);
    virtual void setParameters(TH1* /*h*/);
    virtual void setParameters(TH2* /*h*/);

    /// Fill histogram from data
    virtual void fillHisto(json& /*j*/);
    virtual void fillHisto(TH1* /*h*/);
    virtual void fillHisto(TH2* /*h*/);
};

#endif //__HISTO2D_H
